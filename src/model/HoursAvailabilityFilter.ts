export class HoursAvailabilityFilter {

    constructor(
        public dateIn: string,
        public hairdresserId: number
    ) {

    }
}