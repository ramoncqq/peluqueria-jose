export class AvailabilityFilter {

    constructor(
        public dateFrom: string,
        public dateTo: string,
        public hairdresser
    ){}
}