export class AvailabilityAppointment {

    constructor(
        public day,
        public totalAppointments: number,
        public full: boolean,
        public holiday: boolean
    ){}
}