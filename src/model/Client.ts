export class Client {

    constructor(
        public email: string,
        public phone: string,
        public name: string,
        public surname: string,
        public active: boolean,
        public hairdresserId: number,
        public fcmToken: string,
        public notification: boolean
    ) { }
}