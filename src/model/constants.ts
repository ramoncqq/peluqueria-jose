const ENDPOINT_BASE: string = "http://localhost:1515";
const ENDPOINT_APPOINTMENT: string = ENDPOINT_BASE + "/appointment";
const ENDPOINT_CLIENT: string = ENDPOINT_BASE + "/client";
const ENDPOINT_HAIRDRESSER: string = ENDPOINT_BASE + "/hairdresser";
const ENDPOINT_AVAILABILITY: string = ENDPOINT_BASE + "/availability";
export const ENDPOINT_HOLIDAY: string = ENDPOINT_BASE + "/holiday";

// ENDPOINTS DE LA APLICACIÓN
export const ENDPOINT_LOGIN: string = ENDPOINT_BASE + "/login";
export const ENDPOINT_REFRESH: string = ENDPOINT_BASE + "/refresh";
export const ENDPOINT_HAIRDRESSER_APPOINTMENT: string = ENDPOINT_HAIRDRESSER + "/appointment";
export const ENDPOINT_AVAILABILITY_APPOINTMENTS: string = ENDPOINT_AVAILABILITY + "/dates/";
export const ENDPOINT_AVAILABILITY_HOURS: string = ENDPOINT_AVAILABILITY + "/hours";
export const ENDPOINT_CREATE_APPOINTMENT: string = ENDPOINT_APPOINTMENT;
export const ENDPOINT_CANCEL_APPOINTMENT: string = ENDPOINT_APPOINTMENT + "/cancel/";
export const ENDPOINT_CANCEL_MASSIVE_APPOINTMENT: string = ENDPOINT_APPOINTMENT + "/cancelMassive/";
export const ENDPOINT_FINISH_APPOINTMENT: string = ENDPOINT_APPOINTMENT + "/finish/";
export const ENDPOINT_APPOINTMENT_BY_EMAIL: string = ENDPOINT_CLIENT + "/appointment/email/";
export const ENDPOINT_APPOINTMENT_BY_PHONE: string = ENDPOINT_CLIENT + "/appointment/phone/";
export const ENDPOINT_MODIFY_CLIENT: string = ENDPOINT_CLIENT;
export const ENDPOINT_SAVE_CLIENT: string = ENDPOINT_CLIENT + "/create";
export const ENDPOINT_EXIST_CLIENT: string = ENDPOINT_CLIENT + "/exists/";
export const ENDPOINT_LOCK_CLIENT: string = ENDPOINT_CLIENT + "/lock/";
export const ENDPOINT_UNLOCK_CLIENT: string = ENDPOINT_CLIENT + "/unlock/";
export const ENDPOINT_CLIENT_BY_PHONE: string = ENDPOINT_CLIENT + "/filter/";
export const ENDPOINT_HAIRDRESSERS: string = ENDPOINT_HAIRDRESSER + "/";
export const ENDPOINT_HAIRDRESSER_ID: string = ENDPOINT_HAIRDRESSER + "/";

export const TAB_APPOINTMENTS_ADMIN: number = 0;
export const TAB_APPOINTMENTS_USER: number = 2;
export const TAB_CREATE_APPOINTMENT_USER: number = 3;
export const TIME_ALERT: number = 3000;
export const STATE_CANCEL: string = "CANCELADA";
export const STATE_FINISH: string = "FINALIZADA";
export const MAX_TIME_APPOINTMENTS_USER: number = 2;
export const MAX_TIME_APPOINTMENTS_ADMIN: number = 5;
export const FORMAT_DATE: string = 'YYYY-MM-DD';
export const FORMAT_DATE_WITH_MINUTE: string = 'YYYY-MM-DD HH:mm:ss';
export const FORMAT_DATE_FRONT: string = 'DD/MM/YYYY';
export const FORMAT_DATE_APPOINTMENTS: string = 'DD MMMM YYYY HH:mm';

// Propiedades de seguridad
export const KEY_TOKEN: string = "token";
export const MAX_TIME_TOKEN: number = 2;
export const WHITE_LIST_URL : string[] = ['/login', '/refresh', '/client/exists', './assets/i18n/', '/client/create'];


