export class AppointmentCreate {

    constructor(
        public date : string,
        public email: string,
        public hairdresser: number,
        public service: number
    ){}
}