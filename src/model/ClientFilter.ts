export class ClientFilter {

    constructor(
        public email: string,
        public name: string,
        public surname: string
    ) { }
}