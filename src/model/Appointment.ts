export class Appointment {

    constructor(
        public date: string,
        public hairdresserName: string,
        public serviceName: string,
        public state: string,
        public email: string,
        public id: number
    ) { }

}