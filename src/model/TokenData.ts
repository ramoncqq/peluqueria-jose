
export class TokenData {

    constructor(
        public sub: string,
        public exp: number,
        public authorities: string
    ) {

    }
}