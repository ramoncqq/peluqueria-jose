export class HairdresserAppointmentFilter {

    constructor(
        public hairdresserId: number,
        public dateFrom: string
    ) { }
}