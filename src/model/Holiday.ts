export class Holiday {

    constructor(
        public startDate: string,
        public hairdresserId: number
    ) {}
}