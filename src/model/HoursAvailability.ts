export class HoursAvailability {

    constructor(
        public morning: Array<string>,
        public afternoon: Array<string>
    ) { }
}