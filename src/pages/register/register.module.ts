import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormClientModule } from '../../components/form-client/form-client.module';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    HeaderModule,
    FormClientModule,
    TranslateModule
  ],
  entryComponents: [
    RegisterPage
  ]
})
export class RegisterPageModule { }
