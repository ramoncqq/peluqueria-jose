import { Component, OnInit } from '@angular/core';
import { Client } from '../../model/Client';
import { NavParams, NavController } from 'ionic-angular';
import { ClientServiceProvider } from '../../providers/client-service';
import { NotificationServiceProvider } from '../../providers/notification-service';
import { FormGroup } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service';
import { FcmServiceProvider } from '../../providers/fcm';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { SpinnerServiceProvider } from '../../providers/spinner-service';

@IonicPage({
  name: 'RegisterPage'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {

  client: Client;

  constructor(
    private navParams: NavParams,
    private clientService: ClientServiceProvider,
    private notificationService: NotificationServiceProvider,
    private authService: AuthServiceProvider,
    private fcm: FcmServiceProvider,
    private spinnerService: SpinnerServiceProvider
  ) { }

  ngOnInit() {
    this.client = new Client(this.navParams.get("email"), null, null, null, null, null, null, null);
  }

  createClient(formClient: FormGroup) {
    let client: Client = formClient.value;
    // Se obtiene el token de FB y se guarda
    this.fcm.getToken().then(token => {
      console.log(`TOKEN en el registro: ${token}`);
      this.fcm.saveTokenToFirestore(token, client.email);
      // Se guarda el cliente en la BB.DD.
      client.fcmToken = token;
      this.clientService.createClient(client).subscribe(data => {
        this.client = data;
        formClient.reset();
        this.notificationService.showAlert('CREATE_CLIENT_TITLE', 'CREATE_CLIENT_DESCRIPTION', this.getTokenAndRedirect.bind(this));
      }, () => { }, () => {
        this.spinnerService.hide();
      });
    });
  }

  /**
   * Método que hace el login y el redirect
   */
  getTokenAndRedirect() {
    this.authService.loginAndRedirect(this.client);
  }

}
