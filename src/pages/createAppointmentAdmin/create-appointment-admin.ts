import { Component, OnInit } from '@angular/core';
import { NavParams, IonicPage } from 'ionic-angular';
import { AppointmentServiceProvider } from '../../providers/appointment-service';
import { HairdresserServiceProvider } from '../../providers/hairdresser-service';
import * as Constants from '../../model/constants';
import * as moment from 'moment';
import { Service } from '../../model/Service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppointmentCreate } from '../../model/AppointmentCreate';
import { NotificationServiceProvider } from '../../providers/notification-service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { CalendarServiceProvider } from '../../providers/calendar-service';
import { AvailabilityServiceProvider } from '../../providers/availabity-service';
import { Client } from '../../model/Client';
import { CalendarResult } from 'ion2-calendar';
import { ClientServiceProvider } from '../../providers/client-service';
import { ClientFilter } from '../../model/ClientFilter';
import { HoursAvailabilityFilter } from '../../model/HoursAvailabilityFilter';
import { HoursAvailability } from '../../model/HoursAvailability';
import { SpinnerServiceProvider } from '../../providers/spinner-service';

@IonicPage()
@Component({
    selector: 'page-create-appoingment-admin',
    templateUrl: 'create-appointment-admin.html'
})
export class AppointmentCreateAdminPage implements OnInit {

    formCreateAppointment: FormGroup;
    clientFind: ClientFilter;
    client: Client;
    serviceList: Service[];
    serviceId: number;
    appointmentCreate: AppointmentCreate;
    dateSelected: string;
    hoursAvailability: HoursAvailability;
    phone: string;
    emailFind: string;
    allHours: string[];
    nameClient: string;

    constructor(
        private navParams: NavParams,
        private formBuilder: FormBuilder,
        private hairdresserService: HairdresserServiceProvider,
        private calendarService: CalendarServiceProvider,
        private availabilityService: AvailabilityServiceProvider,
        private appointmentService: AppointmentServiceProvider,
        private notificationService: NotificationServiceProvider,
        private clientService: ClientServiceProvider,
        private navCtrl: NavController,
        private spinnerService: SpinnerServiceProvider) {
    }

    // Funcion que obtiene al cliente y obtiene los servicios de un peluquero
    ngOnInit() {
        this.client = this.navParams.get("client");
        this.hairdresserService.getHairdresser(this.client.hairdresserId).subscribe(data => {
            this.serviceList = data.services
        });
        this.formCreateAppointment = this.initializeForm();
    }

    // Función que cada vez que carga la página obtiene las citas
    ionViewWillEnter() {
        this.resetForm();
    }

    // Funcion que abre el calendario y se define el click del calendario
    openCalendar() {
        let modal = this.calendarService.openCalendar(Constants.MAX_TIME_APPOINTMENTS_ADMIN, this.availabilityService.getAvailabilityList());
        modal.onDidDismiss((date: CalendarResult, type: string) => {
            if (date != null) {
                this.dateSelected = moment(date.dateObj).format(Constants.FORMAT_DATE_FRONT);
                this.getAvailabilityHours(new HoursAvailabilityFilter(date.string, this.client.hairdresserId));
            }
        });
    }

    // función que guarda la disponibilidad
    setAvailability() {
        this.availabilityService.setAvailabilityList(Constants.MAX_TIME_APPOINTMENTS_ADMIN, this.client.hairdresserId);
    }

    // función que crea la cita.
    createAppointment() {
        let appointmentCreateForm: AppointmentCreate = this.formCreateAppointment.value;
        let dateWithHour = moment(appointmentCreateForm.date, Constants.FORMAT_DATE_FRONT).format(Constants.FORMAT_DATE) + " " + this.formCreateAppointment.get('hourAvailable').value;
        let appointService: AppointmentCreate = new AppointmentCreate(dateWithHour, appointmentCreateForm.email, appointmentCreateForm.hairdresser, appointmentCreateForm.service);
        this.appointmentService.createAppointment(appointService, true).subscribe(data => {
            this.spinnerService.show();
            this.appointmentCreate = data;
            this.clientFind = null;
            this.notificationService.showAlert('CREATE_APPOINTMENT_TITLE', 'CREATE_APPOINTMENT_DESCRIPTION', this.goToAppointmentUser.bind(this));
        }, () => { }, () => {
            this.spinnerService.hide();
        });
    }

    // función que cuando el teléfono introducido es mayor de 9 obtiene el clietn
    getClientByPhone() {
        if (this.phone.length >= 9) {
            this.clientService.getClientByPhone(this.phone).subscribe(data => {
                this.spinnerService.show();
                this.clientFind = data;
                this.emailFind = data.email;
                this.nameClient = data.name;
            }, err => {
                this.clientFind = null;
                this.emailFind = null;
                this.nameClient = null;
            }, () => {
                this.spinnerService.hide();
            });
        }
    }

    // Función que consulta las horas disponibles del día seleccionado
    private getAvailabilityHours(HoursAvailabilityFilter: HoursAvailabilityFilter) {
        this.availabilityService.getAvailabilityHours(HoursAvailabilityFilter).subscribe(data => {
            this.spinnerService.show();
            this.formCreateAppointment.get('hourAvailable').reset();
            this.hoursAvailability = data;
            this.allHours = this.hoursAvailability.morning.concat(this.hoursAvailability.afternoon);
        }, () => { }, () => {
            this.spinnerService.hide();
        });
    }

    // Función que inicializa el formulario
    private initializeForm() {
        return this.formBuilder.group({
            name: ['', Validators.required],
            date: [''],
            hairdresser: [this.client.hairdresserId, Validators.required],
            service: ['', Validators.required],
            email: ['', Validators.required],
            hourAvailable: ['', Validators.required]
        });
    }

    // Función que redirecciona a la pantalla de mis citas
    private goToAppointmentUser() {
        this.resetForm();
        this.navCtrl.parent.select(Constants.TAB_APPOINTMENTS_ADMIN);
    }

    private resetForm(){
        this.formCreateAppointment.get('name').reset();
        this.formCreateAppointment.get('date').reset();
        this.formCreateAppointment.get('service').reset();
        this.formCreateAppointment.get('email').reset();
        this.formCreateAppointment.get('hourAvailable').reset();
        this.phone = null;
    }
}