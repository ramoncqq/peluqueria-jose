import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentCreateAdminPage } from './create-appointment-admin';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from '../../components/header/header.module';
import { LimitToModule } from '../../directives/limit-to/limit-to.module';

@NgModule({
  declarations: [
    AppointmentCreateAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentCreateAdminPage),
    TranslateModule,
    LimitToModule,
    HeaderModule
  ],
  entryComponents: [
    AppointmentCreateAdminPage
  ]
})
export class AppointmentCreateAdminPageModule { }
