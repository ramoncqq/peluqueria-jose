import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, IonicPage } from 'ionic-angular';
import { AppointmentServiceProvider } from '../../providers/appointment-service';
import { HairdresserServiceProvider } from '../../providers/hairdresser-service';
import * as Constants from '../../model/constants';
import * as moment from 'moment';
import { Hairdresser } from '../../model/Hairdresser';
import { Service } from '../../model/Service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppointmentCreate } from '../../model/AppointmentCreate';
import { NotificationServiceProvider } from '../../providers/notification-service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { AvailabilityServiceProvider } from '../../providers/availabity-service';
import { HoursAvailabilityFilter } from '../../model/HoursAvailabilityFilter';
import { HoursAvailability } from '../../model/HoursAvailability';
import { CalendarServiceProvider } from '../../providers/calendar-service';
import { CalendarResult } from 'ion2-calendar';
import { SpinnerServiceProvider } from '../../providers/spinner-service';

@IonicPage()
@Component({
  selector: 'page-appointment-create',
  templateUrl: 'appointment-create.html'
})
export class AppointmentCreatePage implements OnInit {

  @ViewChild('createAppointmentSlider') createAppointmentSlider: any;

  hourAvailability: HoursAvailability = new HoursAvailability([], []);
  hairdresserList: Hairdresser[];
  serviceList: Array<Service>;
  formCreateAppointment: FormGroup;
  maxTime: number;
  service: Service;
  hairdresser: Hairdresser;

  // Variables para las fechas
  fullDate: string;
  dateBack: string;
  dateSelected: string;

  constructor(
    private appointmentService: AppointmentServiceProvider,
    private navParams: NavParams,
    private hairdresserService: HairdresserServiceProvider,
    private formBuilder: FormBuilder,
    private notificationService: NotificationServiceProvider,
    private navCtrl: NavController,
    private availabilityService: AvailabilityServiceProvider,
    private calendarService: CalendarServiceProvider,
    private spinnerService: SpinnerServiceProvider) {
  }

  ngOnInit() {
    this.maxTime = Constants.MAX_TIME_APPOINTMENTS_USER;
    this.formCreateAppointment = this.initializeForm_slider_1();
    this.getHairdressers();
    this.createAppointmentSlider.lockSwipes(true);
  }

  ionViewWillEnter() {
    this.goToFirstSlide();
  }

  // Función que setea la disponibilidad, inicializa el calendario y pasa al siguiente paso
  public setAvailabilityAndNextSlide() {
    this.availabilityService.setAvailabilityList(Constants.MAX_TIME_APPOINTMENTS_USER, this.hairdresser.id);
    this.nextSlide();
  }

  public nextSlider3(hourSelected: string) {
    this.dateBack = this.dateBack.concat(" " + hourSelected);
    this.fullDate = moment(this.dateBack).format(Constants.FORMAT_DATE_APPOINTMENTS);
    this.nextSlide();
  }

  // Función que obtiene los servicios de un peluquero
  public getServicesByHairdresser() {
    this.formCreateAppointment.get('service').reset();
    this.serviceList = this.hairdresserList.filter(hairdresser => hairdresser.id == this.hairdresser.id)[0].services;
  }

  // Funcion que crea la cita y cuando se crea limpia el formulario y navega hacia la pantalla de las citas.
  public createAppointment() {
    let appointment: AppointmentCreate = new AppointmentCreate(this.dateBack, this.navParams.get('client').email,
      this.hairdresser.id, this.service.id);
    this.appointmentService.createAppointment(appointment, false).subscribe(data => {
      this.spinnerService.show();
      this.notificationService.showAlert('CREATE_APPOINTMENT_TITLE', 'CREATE_APPOINTMENT_DESCRIPTION', this.goToAppointmentUser.bind(this));
    }, () => { }, () => {
      this.spinnerService.hide();
    });
  }

  public goToFirstSlide() {
    this.formCreateAppointment.reset();
    this.dateSelected = "";
    this.dateBack = "";
    this.fullDate = "";
    this.hourAvailability = new HoursAvailability([], []);
    this.createAppointmentSlider.lockSwipes(false);
    this.createAppointmentSlider.slideTo(0);
    this.createAppointmentSlider.lockSwipes(true);
  }

  // Función que consulta las horas disponibles del día seleccionado
  public getAvailabilityHours() {
    let filterAvailability: HoursAvailabilityFilter = new HoursAvailabilityFilter(this.dateBack, this.hairdresser.id);
    this.availabilityService.getAvailabilityHours(filterAvailability).subscribe(data => {
      this.spinnerService.show();
      this.hourAvailability = data;
    }, () => { }, () => {
      this.spinnerService.hide();
    });
  }

  // Funcion que abre el calendario y se define el click del calendario
  openCalendar() {
    let modal = this.calendarService.openCalendar(Constants.MAX_TIME_APPOINTMENTS_USER, this.availabilityService.getAvailabilityList());
    modal.onDidDismiss((date: CalendarResult, type: string) => {
      if (date != null) {
        this.dateSelected = moment(date.string).format(Constants.FORMAT_DATE_FRONT);
        this.dateBack = moment(this.dateSelected, Constants.FORMAT_DATE_FRONT).format(Constants.FORMAT_DATE);
        this.getAvailabilityHours();
      }
    });
  }

  // Función que obtiene los peluqueros y los servicios
  private getHairdressers() {
    this.hairdresserService.getHairdressers().subscribe(data => {
      this.hairdresserList = data;
    });
  }

  private initializeForm_slider_1() {
    return this.formBuilder.group({
      hairdresser: ['', Validators.required],
      service: ['', Validators.required],
      email: [{ value: this.navParams.get('client').email, disabled: true }, Validators.required]
    });
  }

  private goToAppointmentUser() {
    this.goToFirstSlide();
    this.navCtrl.parent.select(Constants.TAB_APPOINTMENTS_USER);
  }

  public nextSlide() {
    this.createAppointmentSlider.lockSwipes(false);
    this.createAppointmentSlider.slideNext();
    this.createAppointmentSlider.lockSwipes(true);
  }


}
