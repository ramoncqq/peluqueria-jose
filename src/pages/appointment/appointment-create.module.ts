import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentCreatePage } from './appointment-create';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AppointmentCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentCreatePage),
    TranslateModule,
    HeaderModule
  ],
  entryComponents: [
    AppointmentCreatePage,
  ]
})
export class AppointmentCreatePageModule { }
