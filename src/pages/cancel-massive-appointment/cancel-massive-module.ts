import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { CancelAppointmentMassivePage } from './cancel-massive-appointment';
import { CalendarCustomModule } from '../../components/calendar/calendar.module';

@NgModule({
  declarations: [
    CancelAppointmentMassivePage,
  ],
  imports: [
    IonicPageModule.forChild(CancelAppointmentMassivePage),
    TranslateModule,
    CalendarCustomModule,
    HeaderModule
  ],
  entryComponents: [
    CancelAppointmentMassivePage
  ]
})
export class CancelMassivePageModule {}
