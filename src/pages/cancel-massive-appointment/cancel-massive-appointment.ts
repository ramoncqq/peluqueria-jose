import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavParams, Form } from "ionic-angular";
import { AppointmentServiceProvider } from "../../providers/appointment-service";
import { Client } from "../../model/Client";
import { IonicPage } from "ionic-angular/navigation/ionic-page";
import { SpinnerServiceProvider } from '../../providers/spinner-service';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NotificationServiceProvider } from "../../providers/notification-service";
import { AppointmentCancelMassive } from "../../model/AppointmentCancelMassive";

IonicPage()
@Component({
    selector: 'page-cancel-massive-appointment',
    templateUrl: 'cancel-massive-appointment.html'
})
export class CancelAppointmentMassivePage {

    constructor(
        private navParams: NavParams,
        private fb: FormBuilder,
        private appointmentService: AppointmentServiceProvider,
        private notificationService : NotificationServiceProvider,
        private spinnerService: SpinnerServiceProvider) {
    }

    public form: FormGroup;
    @ViewChild('areaReason') areaReason: ElementRef;

    ngOnInit() {
        let client: Client = this.navParams.get('client');
        this.form = this.fb.group({
            dateCancelation: ["", Validators.required],
            reason: ["", Validators.required],
            hairdresserId: [client.hairdresserId, Validators.required],
        })
    }

    /**
     * Método que setea la fecha seleccionada.
     * @param dateEvent 
     */
    setDate(dateEvent: string) {
        this.form.get('dateCancelation').setValue(dateEvent);
    }

    resize() {
        let element = this.areaReason['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        let scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.areaReason['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }

    // Función que muestra la confirmación para cancelar la citas
    showConfirmCancelAppointments() {
        this.notificationService.showConfirm('BUTTON-CANCEL-MASIVE-APPONINTMENT', 'FINISH-APPOINTMENT-MASSIVE-MESSAGE', this.cancelAllAppointments.bind(this));
    }

    /**
     * Método que cancela todas las citas de ese día.
     */
    cancelAllAppointments() {
        let appointmentCancelMassive : AppointmentCancelMassive = new AppointmentCancelMassive();
        appointmentCancelMassive.dateCancelation = this.form.get('dateCancelation').value;
        appointmentCancelMassive.hairdresserId = this.form.get('hairdresserId').value;
        appointmentCancelMassive.reason = this.form.get('reason').value;

        this.spinnerService.show();
        this.appointmentService.cancelAllAppointments(appointmentCancelMassive).subscribe(data => {
            this.notificationService.showAlertToast('FINISH-APPOINTMENT-MASSIVE-MESSAGE-OK', true);
        }, () => { }, () => {
            this.spinnerService.hide();
        });
    }
}