import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentAdminPage } from './appointment-admin';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppointmentCardModule } from '../../components/appoinment-card/appointment-card-module';
import { LimitToModule } from '../../directives/limit-to/limit-to.module';

@NgModule({
  declarations: [
    AppointmentAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentAdminPage),
    TranslateModule,
    AppointmentCardModule,
    LimitToModule,
    HeaderModule
  ],
  entryComponents: [
    AppointmentAdminPage
  ]
})
export class AppointmentAdminPageModule {}
