import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";
import { Appointment } from "../../model/Appointment";
import * as Constants from '../../model/constants';
import * as moment from 'moment';
import { Client } from "../../model/Client";
import { HairdresserAppointmentFilter } from "../../model/HairdresserApointentFilter";
import { HairdresserServiceProvider } from "../../providers/hairdresser-service";
import { AppointmentServiceProvider } from "../../providers/appointment-service";
import { NotificationServiceProvider } from "../../providers/notification-service";
import { CalendarServiceProvider } from "../../providers/calendar-service";
import { CalendarResult } from "ion2-calendar";
import { IonicPage } from "ionic-angular/navigation/ionic-page";
import { SpinnerServiceProvider } from '../../providers/spinner-service';

@IonicPage()
@Component({
  selector: 'page-appointment-admin',
  templateUrl: 'appointment-admin.html'
})
export class AppointmentAdminPage {

  appointments: Array<Appointment>;
  client: Client;
  idAppointment: number;
  phone: string;

  constructor(
    private navParams: NavParams,
    private appointmentService: AppointmentServiceProvider,
    private hairdresserService: HairdresserServiceProvider,
    private notificationService: NotificationServiceProvider,
    private calendarService: CalendarServiceProvider,
    private spinnerService: SpinnerServiceProvider) {
  }

  // Función que cada vez que carga la página obtiene las citas
  ionViewWillEnter() {
    this.phone = null ;
    this.client = this.navParams.get('client');
    let filter = new HairdresserAppointmentFilter(this.client.hairdresserId, moment().format(Constants.FORMAT_DATE));
    this.getAppointmentsByHairdresserAndDate(filter);
  }

  // Función que obtiene las citas en función de la fecha y peluquero
  getAppointmentsByHairdresserAndDate(filter: HairdresserAppointmentFilter) {
    this.hairdresserService.getAppointmentsByHairdresserAndDate(filter).subscribe(data => {
      data.forEach(appointment => {
        appointment.date = moment(appointment.date).locale('es').format(Constants.FORMAT_DATE_APPOINTMENTS);
      });
      this.appointments = data;
    });
  }

  // Funcion que busca las citas por teléfono
  getAppointmentsByPhone() {
    if (this.phone.length >= 9) {
      this.appointmentService.getAppointmentByPhone(this.phone).subscribe(data => {
        this.spinnerService.show();
        data.forEach(appointment => {
          appointment.date = moment(appointment.date).locale('es').format(Constants.FORMAT_DATE_APPOINTMENTS);
        });
        this.appointments = data;
      }, () => { }, () => {
        this.spinnerService.hide();
      });
    }
  }

  // Funcion que abre el calendario y obtiene las citas para el dia seleccionado
  openCalendar() {
    let modal = this.calendarService.openCalendar(Constants.MAX_TIME_APPOINTMENTS_ADMIN);
    modal.onDidDismiss((date: CalendarResult, type: string) => {
      if (date != null) {
        let filter = new HairdresserAppointmentFilter(this.client.hairdresserId, moment(date.dateObj).format(Constants.FORMAT_DATE));
        this.getAppointmentsByHairdresserAndDate(filter);
      }
    });
  }

  showConfirmFinishAppointment(idAppointment: number) {
    this.idAppointment = idAppointment;
    this.notificationService.showConfirm('FINISH-APPOINTMENT', 'FINISH-APPOINTMENT-MESSAGE', this.finishAppointment.bind(this));
  }

  // Función que finaliza la cita
  finishAppointment() {
    this.appointmentService.finishAppointment(this.idAppointment).subscribe(data => {
      this.spinnerService.show();
      this.appointments.filter(appointment => appointment.id === this.idAppointment)[0].state = Constants.STATE_FINISH;
      this.notificationService.showAlertToast('FINISH-APPOINTMENT-MESSAGE-OK', true);
    }, () => { }, () => {
      this.spinnerService.hide();
    });
  }

}