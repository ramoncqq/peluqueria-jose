import { Component } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { FacebookResponse } from '../../model/FacebookResponse';
import { NavController, IonicPage } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { GoogleResponse } from '../../model/GoogleResponse';
import { ClientServiceProvider } from '../../providers/client-service';
import { NotificationServiceProvider } from '../../providers/notification-service';
import { Client } from '../../model/Client';
import { AuthServiceProvider } from '../../providers/auth-service';
import * as Constants from '../../model/constants';
import { SpinnerServiceProvider } from '../../providers/spinner-service';

@IonicPage({
  name: 'LoginPage'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  client: Client;
  emailSelected: string;

  constructor(
    private facebook: Facebook,
    private navCtrl: NavController,
    private googlePlus: GooglePlus,
    private clientService: ClientServiceProvider,
    private notificationService: NotificationServiceProvider,
    private authService: AuthServiceProvider,
    private spinnerService: SpinnerServiceProvider
  ) { }

  /**
   * Función que hace el login de facebook
   */
  loginFacebook() {
    let fcbResponse: FacebookResponse = new FacebookResponse();
    this.spinnerService.show();
    this.facebook.login(['public_profile', 'email'])
      .then(res => {
        if (res.status == "connected") {
          this.facebook.api("/me?fields=name,email", []).then((user) => {
            fcbResponse.name = user.name;
            fcbResponse.email = user.email;
            this.checkClientExist(fcbResponse.email);
          });
        }
      })
      .catch(error => {
        this.spinnerService.hide()
        console.error("Error FACEBOOK:" + error);
      });
  }

  /**
   * Función que hace el login de google
   */
  loginGoogle() {
    let googleResponse: GoogleResponse = new GoogleResponse();
    this.spinnerService.show();
    this.googlePlus.login({})
      .then(res => {
        googleResponse.email = res.email;
        googleResponse.id = res.userId;
        this.checkClientExist(googleResponse.email);
      })
      .catch(error => {
        this.spinnerService.hide()
        console.error("Error GOOGLE:" + error);
      });
  }

  // Función que comprueba si el usuario existe o no y si está desbloqueado
  private checkClientExist(email: string) {
    this.clientService.checkClientExists(email).subscribe(client => {
      this.client = client;
      if (client.email != null && client.active == true) {
        // login normal
        this.authService.loginAndRedirect(client);
      } else if (client.email != null && client.active == false) {
        // Muestra el popup para desbloquear al cliente
        this.notificationService.showConfirm('UNLOCK-CLIENT-TITLE', 'UNLOCK-CLIENT-DESCRIPTION', this.unlockClient.bind(this));
      } else if (client.email == null) {
        // redirección a la página de registro
        this.navCtrl.push('RegisterPage', { email: email });
      }
    }, () => { }, () => {
      this.spinnerService.hide();
    });;
  }

  /**
   * Función que obtiene el token y se hace el redirect
   */
  async unlockClient() {
    let token: string;
    await this.authService.login(this.client).subscribe(data => {
      token = data.token;
      this.authService.setToken(token);
    });

    await this.clientService.unlockClient(this.client.email).subscribe(data => {
      this.notificationService.showAlertToast('UNLOCK-CLIENT-DESCRIPTION-OK', true);
      if (this.authService.isAdmin(token)) {
        this.navCtrl.push('TabsPage', { client: data, opentab: Constants.TAB_APPOINTMENTS_ADMIN });
      } else {
        this.navCtrl.push('TabsPage', { client: data, opentab: Constants.TAB_APPOINTMENTS_USER });
      }
    });
  }

  // ELIMINAR
  loginFake() {
    this.checkClientExist(this.emailSelected);
  }

}
