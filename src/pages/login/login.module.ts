import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core'; 

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    TranslateModule,
    HeaderModule
  ],
  entryComponents: [
    LoginPage
  ]
})
export class LoginPageModule { 

  constructor(public translatemodule: TranslateModule){  
  }

}
