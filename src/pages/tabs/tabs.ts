import { Component } from '@angular/core';
import { AppointmentCreatePage } from '../appointment/appointment-create';
import { NavParams, IonicPage } from 'ionic-angular';
import { Client } from '../../model/Client';
import { AppointmentUserPage } from '../appointmentUser/appointment-user';
import { ProfilePage } from '../profile/profile';
import { AppointmentAdminPage } from '../appointmentAdmin/appointment-admin';
import { AppointmentCreateAdminPage } from '../createAppointmentAdmin/create-appointment-admin';
import { AuthServiceProvider } from '../../providers/auth-service';
import { NotWorkDaysPage } from '../not-work-days/not-work-days';
import { CancelAppointmentMassivePage } from '../cancel-massive-appointment/cancel-massive-appointment';

@IonicPage({
  name: 'TabsPage'
})
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  //TABS USER
  tabAppointmentCreate;
  tabAppointmentUser;

  // TABS COMMON
  tabProfile;

  // TABS ADMIN
  tabAppointmentAdmin;
  tabAppointmentCreateAdminPage;
  tabNotWorkDaysPage;
  tabCancelAppointmentMassive;

  client: Client;
  isAdmin: boolean;

  selectedIndex: number;

  constructor(
    public params: NavParams,
    public authService: AuthServiceProvider
  ) {
    this.selectedIndex = params.get('opentab');
    this.client = this.params.data;
    // Se asocian a las variables los componentes
    this.tabAppointmentCreate = AppointmentCreatePage;
    this.tabAppointmentUser = AppointmentUserPage;
    this.tabProfile = ProfilePage;
    this.tabAppointmentAdmin = AppointmentAdminPage;
    this.tabAppointmentCreateAdminPage = AppointmentCreateAdminPage;
    this.tabNotWorkDaysPage = NotWorkDaysPage;
    this.tabCancelAppointmentMassive = CancelAppointmentMassivePage;

    this.authService.getToken().then(data => {
      this.isAdmin = this.authService.isAdmin(data);
    });
  }

}
