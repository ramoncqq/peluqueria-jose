import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsPage } from './tabs';
import { AppointmentAdminPageModule } from '../appointmentAdmin/appointment-admin.module';
import { AppointmentUserPageModule } from '../appointmentUser/appointment-user.module';
import { AppointmentCreateAdminPageModule } from '../createAppointmentAdmin/create-appointment-admin.module';
import { NotWorkDaysPageModule } from '../not-work-days/not-work-days.module';
import { ProfilePageModule } from '../profile/profile.module';
import { AppointmentCreatePageModule } from '../appointment/appointment-create.module';
import { TranslateModule } from '@ngx-translate/core';
import { CancelAppointmentMassivePage } from '../cancel-massive-appointment/cancel-massive-appointment';
import { CancelMassivePageModule } from '../cancel-massive-appointment/cancel-massive-module';

@NgModule({
  declarations: [
    TabsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsPage),
    AppointmentAdminPageModule,
    AppointmentUserPageModule,
    AppointmentCreateAdminPageModule,
    NotWorkDaysPageModule,
    TranslateModule,
    AppointmentCreatePageModule,
    CancelMassivePageModule,
    ProfilePageModule
  ],
  entryComponents: [
    TabsPage
  ]
})
export class TabsPageModule { }
