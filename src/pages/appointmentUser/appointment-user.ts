import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";
import { AppointmentServiceProvider } from "../../providers/appointment-service";
import { Appointment } from "../../model/Appointment";
import * as Constants from '../../model/constants';
import * as moment from 'moment';
import { Client } from "../../model/Client";
import { NavController } from "ionic-angular";
import { IonicPage } from "ionic-angular/navigation/ionic-page";
import { SpinnerServiceProvider } from '../../providers/spinner-service';

IonicPage()
@Component({
  selector: 'page-appointment-user',
  templateUrl: 'appointment-user.html'
})
export class AppointmentUserPage {

  appointments: Array<Appointment>;
  appointmentSelected: Appointment;
  fullname: string;
  client: Client;

  constructor(
    private navParams: NavParams,
    private appointmentService: AppointmentServiceProvider,
    private navCtrl: NavController,
    private spinnerService: SpinnerServiceProvider) {
  }

  // Función que cada vez que carga la página obtiene las citas
  ionViewWillEnter() {
    this.client = this.navParams.get('client');
    this.fullname = this.client.name.concat(" " + this.client.surname);
    this.getAppointmentsByEmail(this.client.email);
  }

  // Función que obtiene las citas en función del email
  getAppointmentsByEmail(email: string) {
    this.appointmentService.getAppointmentsByEmail(email).subscribe(data => {
      data.forEach(appointment => {
        appointment.date = moment(appointment.date).locale('es').format(Constants.FORMAT_DATE_APPOINTMENTS);
      });
      this.appointments = data;
    });
  }

  // Función que redirecciona a solicitar cita (se muestra si no hay ninguna cita)
  navigateToCreateAppointment() {
    this.navCtrl.parent.select(Constants.TAB_CREATE_APPOINTMENT_USER);
  }

}