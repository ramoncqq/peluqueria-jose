import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentUserPage } from './appointment-user';
import { HeaderModule } from '../../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppointmentCardModule } from '../../components/appoinment-card/appointment-card-module';

@NgModule({
  declarations: [
    AppointmentUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentUserPage),
    TranslateModule,
    AppointmentCardModule,
    HeaderModule
  ],
  entryComponents: [
    AppointmentUserPage
  ]
})
export class AppointmentUserPageModule {}
