import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import * as Constants from '../../model/constants';
import { CalendarServiceProvider } from '../../providers/calendar-service';
import { Client } from '../../model/Client';
import { Holiday } from '../../model/Holiday';
import { NotificationServiceProvider } from '../../providers/notification-service';
import { SpinnerServiceProvider } from '../../providers/spinner-service';
import { Months } from '../../model/Month';
import { HolidayServiceProvider } from '../../providers/holiday-service';

const moment = extendMoment(Moment);

@IonicPage()
@Component({
  selector: 'page-not-work-days',
  templateUrl: 'not-work-days.html',
})
export class NotWorkDaysPage implements OnInit {

  dateRange: { from: string; to: string; };
  type: 'string';
  optionsMulti: CalendarComponentOptions;
  client: Client;
  holidays: Array<Holiday>;
  monthSelected: string;
  months: Array<string>;
  daysFilter: number[];
  createHoliday: boolean = true;
  daysSelected: string[];

  constructor(
    private navParams: NavParams,
    private calendarService: CalendarServiceProvider,
    private holidayService: HolidayServiceProvider,
    private notificationService: NotificationServiceProvider,
    private spinnerService: SpinnerServiceProvider
  ) { }

  ngOnInit() {
    this.spinnerService.show();
    this.client = this.navParams.get('client');
    // Obtiene las vacaciones del peluquero
    this.holidayService.getHolidaysHairdresser(this.client.hairdresserId).subscribe(data => {
      this.holidays = data;
      this.initCalendar();
    }, () => { }, () => {

    });
    // Se obtiene el enum de los meses
    this.months = Object.keys(Months);
  }

  // Función que configura el calendario y crea los dias deshabilitados en caso de que existan vacaciones
  initCalendar() {
    let daysConfigCalendar: DayConfig[] = [];

    // En caso de que existan rangos de vacaciones
    this.holidays.forEach(holiday => {
      daysConfigCalendar.push({ date: moment(holiday.startDate).toDate(), disable: true, cssClass: 'dayNotAvailability' });
    });

    this.optionsMulti = {
      pickMode: 'range',
      showAdjacentMonthDay: false,
      weekStart: 1,
      daysConfig: daysConfigCalendar,
      weekdays: this.calendarService.getDaysWeek(),
      monthPickerFormat: this.calendarService.getMonths(),
      showMonthPicker: false,
      to: moment().add(3, 'M').toDate()
    };
    this.spinnerService.hide();
  }

  // Crea el dia de vacaciones con los dias seleccionados del calendario
  createHolidays() {
    let holidayList: Holiday[] = [];
    // Por cada día del rango se crea el objeto del día de de vacaciones
    let rangeDays = Array.from(moment.range(moment(this.dateRange.from).toDate(), moment(this.dateRange.to).toDate()).by("days"));
    rangeDays.map(day => {
      holidayList.push(new Holiday(moment(day).format(Constants.FORMAT_DATE), this.client.hairdresserId));
    });

    this.holidayService.createHolidays(holidayList).subscribe(data => {
      this.spinnerService.show();
      this.refreshHolidays();
    }, () => { }, () => {
      this.spinnerService.hide();
    });
  }

  /**
   * Metodo que en función del mes seleccionado
   */
  filtersDays() {
    this.daysFilter = [];
    let days_list_filter = this.holidays
      .filter(day => (moment(day.startDate, Constants.FORMAT_DATE).month() + 1) === Number(this.monthSelected))
      .map(day => moment(day.startDate, Constants.FORMAT_DATE).date());
    // Se orden los días
    this.daysFilter = days_list_filter.sort((n1, n2) => n1 - n2);
  }

  // Función que muestra la confirmación para cancelar la cita
  showConfirmCancelHolidays() {
    this.notificationService.showConfirm('CANCEL-HOLIDAY', 'CANCEL-HOLIDAY-MESSAGE', this.cancelHolidays.bind(this));
  }

  // Función que cancela las vacaciones
  cancelHolidays() {
    let holidayList: Holiday[] = [];
    this.daysSelected.forEach(day => {
      let monthString = (Number(this.monthSelected) < 10) ? "0" + this.monthSelected : this.monthSelected;
      let startDate = moment().year() + "-" + monthString + "-" + day;
      holidayList.push(new Holiday(startDate, this.client.hairdresserId))
    })

    this.holidayService.cancelHolidays(holidayList).subscribe(data => {
      this.notificationService.showAlertToast('CANCEL-HOLIDAY-MESSAGE-OK', true);
      this.refreshHolidays()

      this.resetCancelHoliday();
    });
  }

  resetCancelHoliday() {
    this.daysSelected = [];
    this.daysFilter = [];
    this.monthSelected = "";
  }

  refreshHolidays() {
     // Obtiene las vacaciones del peluquero
     this.holidayService.getHolidaysHairdresser(this.client.hairdresserId).subscribe(data => {
      this.holidays = data;
      this.initCalendar();
      this.dateRange = null;
    });
  }

}
