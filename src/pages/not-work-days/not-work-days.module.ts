import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotWorkDaysPage } from './not-work-days';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from '../../components/header/header.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  declarations: [
    NotWorkDaysPage,
  ],
  imports: [
    IonicPageModule.forChild(NotWorkDaysPage),
    TranslateModule,
    HeaderModule,
    CalendarModule
    
  ],
  entryComponents: [
    NotWorkDaysPage
  ]
})
export class NotWorkDaysPageModule {}
