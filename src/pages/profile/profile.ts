import { Component, OnInit } from "@angular/core";
import { NavParams, App } from "ionic-angular";
import { ClientServiceProvider } from "../../providers/client-service";
import { NotificationServiceProvider } from "../../providers/notification-service";
import { Client } from "../../model/Client";
import { FormGroup } from "@angular/forms";
import { AuthServiceProvider } from "../../providers/auth-service";
import { IonicPage } from "ionic-angular/navigation/ionic-page";
import { SpinnerServiceProvider } from '../../providers/spinner-service';
import { FcmServiceProvider } from "../../providers/fcm";

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit {

    client: Client;

    constructor(
        private navParams: NavParams,
        private clientService: ClientServiceProvider,
        private notificationService: NotificationServiceProvider,
        private app: App,
        private authService: AuthServiceProvider,
        private spinnerService: SpinnerServiceProvider,
        private fcm: FcmServiceProvider
    ) { }

    ngOnInit() {
        this.client = this.navParams.get('client');
    }

    showConfirmLockClient() {
        this.notificationService.showConfirm('BUTTON-LOCK-CLIENT', 'LOCK-CLIENT-DESCRIPTION',
            this.lockClient.bind(this));
    }

    // Función que bloquea el usuario
    lockClient() {
        this.clientService.lockClient(this.client.email).subscribe(data => {
            this.notificationService.showAlertToast('LOCK-CLIENT-DESCRIPTION-OK', true);
            this.authService.deleteToken();
            this.app.getRootNav().setRoot('LoginPage');
        });
    }

    modifyClient(formClient: FormGroup) {
        let clientModify: Client = formClient.value;
        this.fcm.getToken().then(token => {
            this.spinnerService.show();
            console.log(`TOKEN EN LA MODIFICACIÓN: ${token}`);
            clientModify.fcmToken = token;
            this.clientService.modifyClient(clientModify).subscribe(data => {
                this.client = data;
                this.notificationService.showAlertToast('MODIFY-CLIENT-OK', true);
            }, () => { }, () => {
                this.spinnerService.hide();
            });
        });
    }

}