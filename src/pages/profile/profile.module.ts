import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule } from '../../components/header/header.module';
import { FormClientModule } from '../../components/form-client/form-client.module';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    TranslateModule,
    FormClientModule,
    HeaderModule
  ],
  entryComponents: [
    ProfilePage
  ]
})
export class ProfilePageModule {}
