import { Injectable } from "@angular/core";
import { AppointmentServiceProvider } from "./appointment-service";
import * as moment from 'moment';
import * as Constants from '../model/constants';
import { AvailabilityFilter } from "../model/AvailabilityFilter";
import { AvailabilityAppointment } from "../model/AvailabilityAppointment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { HoursAvailabilityFilter } from "../model/HoursAvailabilityFilter";
import { HoursAvailability } from "../model/HoursAvailability";
import { SpinnerServiceProvider } from "./spinner-service";

@Injectable()
export class AvailabilityServiceProvider {

    private availabilityList: AvailabilityAppointment[];

    constructor(
        private httpClient: HttpClient,
        private spinnerService: SpinnerServiceProvider
    ) { }

    // Función que consulta la disponibilidad de las citas del mes
    setAvailabilityList(maxTimeAppointment: number, hairdresserId: number) {
        let today = moment().format(Constants.FORMAT_DATE);
        let future = moment().add(maxTimeAppointment, 'M').format(Constants.FORMAT_DATE);
        let avilabilityFilter: AvailabilityFilter = new AvailabilityFilter(today, future, hairdresserId);

        // Se llama al servicio preguntadole por la disponobilidad de citas entre 2 fechas
        this.getAvailabilityAppointmets(avilabilityFilter).subscribe(data => {
            this.spinnerService.show();
            this.availabilityList = data;
        }, () => { }, () => {
            this.spinnerService.hide();
        });
    }

    /**
     *  Función que obtiene la lista de las disponiblidades
     */
    getAvailabilityList(): AvailabilityAppointment[] {
        return this.availabilityList;
    }

    /**
     * Función que obtiene los horarios disponibles de una fecha
     * @param hoursAvailabilityFilter 
     */
    getAvailabilityHours(hoursAvailabilityFilter: HoursAvailabilityFilter): Observable<HoursAvailability> {
        return this.httpClient.post<HoursAvailability>(Constants.ENDPOINT_AVAILABILITY_HOURS, hoursAvailabilityFilter);
    }

    /**
     * Función que realiza la petición HTTP para la disponibilidad de las citas entre 2 fechas.
     * @param availabilityFiter 
     */
    private getAvailabilityAppointmets(availabilityFiter: AvailabilityFilter): Observable<AvailabilityAppointment[]> {
        return this.httpClient.post<AvailabilityAppointment[]>(Constants.ENDPOINT_AVAILABILITY_APPOINTMENTS, availabilityFiter);
    }

}