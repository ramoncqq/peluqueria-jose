import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationServiceProvider } from './notification-service';
import { AuthServiceProvider } from './auth-service';
import { App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ErrorsHandler implements ErrorHandler {

    constructor(
        private notificatioNService: NotificationServiceProvider,
        private authService: AuthServiceProvider,
        private translateService: TranslateService,
        private app: App
    ) { }

    handleError(error: Error | HttpErrorResponse) {

        if (error instanceof HttpErrorResponse) {
            let httpErrorCode = error.status;
            // Si es un error controlado o no.
            let message = (error.error.description) ? error.error.description : this.translateService.instant('ERROR-REST');
            switch (httpErrorCode) {
                case 401:
                    this.app.getRootNavs()[0].setRoot('LoginPage');
                    console.log('token expirado session');
                    this.authService.deleteToken();
                    this.notificatioNService.showAlertToast('CLOSE-SESSION', true);
                    break;
                default:
                    this.notificatioNService.showAlertToast(message, false);
            }
        }
    }
}