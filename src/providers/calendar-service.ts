import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DayConfig, CalendarModalOptions, CalendarModal } from 'ion2-calendar';
import { ModalController, Modal } from "ionic-angular";
import * as moment from 'moment';
import { AvailabilityAppointment } from "../model/AvailabilityAppointment";

@Injectable()
export class CalendarServiceProvider {

    dateSelected: string;

    constructor(
        private translate: TranslateService,
        private modalCtrl: ModalController
    ) { }

    getConfigCalendar(maxTime: number, daysConfigCalendar: DayConfig[]): Modal {
        // Se configura las opciones del calendario
        let dateTo = moment().add(maxTime - 1, 'M').toDate();
        let optionsCalendar: CalendarModalOptions = {
            showAdjacentMonthDay: false,
            weekStart: 1,
            weekdays: this.getDaysWeek(),
            to: dateTo,
            defaultScrollTo: dateTo,
            daysConfig: daysConfigCalendar,
            closeLabel: this.translate.instant('LABEL_CLOSE'),
            autoDone: true,
            title: this.translate.instant('TITLE_CALENDAR'),
        };

        let myCalendar = this.modalCtrl.create(CalendarModal, {
            options: optionsCalendar
        });

        myCalendar.present();
        return myCalendar;
    }

    public getDaysConfigCalendar(listAvailability?: AvailabilityAppointment[]): DayConfig[] {
        let daysConfigCalendar: DayConfig[] = [];
        if (listAvailability != null) {
            listAvailability.forEach(availability => {
                let dateAppointment: Date = moment(availability.day).toDate();
                if (availability.full || availability.holiday) {
                    daysConfigCalendar.push({ date: dateAppointment, disable: true, cssClass: 'dayNotAvailability' });
                } else {
                    daysConfigCalendar.push({ date: dateAppointment, disable: false, cssClass: 'dayAvailability' });
                }
            });
        }
        return daysConfigCalendar;
    }

    // Función que configura el calendario y lo abre
    openCalendar(maxTime: number, listAvailability?: AvailabilityAppointment[]): Modal {
        return this.getConfigCalendar(maxTime, this.getDaysConfigCalendar(listAvailability));
    }

    // Obtiene los días de la semana
    public getDaysWeek(): Array<string> {
        return [
            this.translate.instant('DOMINGO'),
            this.translate.instant('LUNES'),
            this.translate.instant('MARTES'),
            this.translate.instant('MIERCOLES'),
            this.translate.instant('JUEVES'),
            this.translate.instant('VIERNES'),
            this.translate.instant('SABADO')
        ]
    }

    public getMonths(): string[] {
        return [
            this.translate.instant('ENERO'),
            this.translate.instant('FEBRERO'),
            this.translate.instant('MARZO'),
            this.translate.instant('ABRIL'),
            this.translate.instant('MAYO'),
            this.translate.instant('JUNIO'),
            this.translate.instant('JULIO'),
            this.translate.instant('AGOSTO'),
            this.translate.instant('SEPTIEMBRE'),
            this.translate.instant('OCTUBRE'),
            this.translate.instant('NOVIEMBRE'),
            this.translate.instant('DICIEMBRE')
        ]
    }
}