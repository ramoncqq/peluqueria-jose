import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat';
import { _throw } from 'rxjs/observable/throw';
import { catchError } from 'rxjs/operators';
import { AuthServiceProvider } from './auth-service';
import * as Constants from '../model/constants';
import { ErrorsHandler } from './errors-service';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

    whiteList: Array<string>;

    constructor(
        private authService: AuthServiceProvider,
        private errorHandler: ErrorsHandler) {
        this.whiteList = Constants.WHITE_LIST_URL;
    }

    /**
   * Método que comprueba si la url es pública
   * @param url 
   */
    isUrlPublic(url: string): boolean {
        let result = false;
        this.whiteList.filter(it => {
            if (!result) {
                result = url.includes(it);
            }
        });
        return result;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let clone: HttpRequest<any>;
        let promise = this.authService.getToken();
        return Observable.fromPromise(promise)
            .mergeMap(token => {
                // Se comprueba si la url es pública
                if (this.isUrlPublic(request.url)) {
                    clone = request;
                } else {
                    clone = request.clone({
                        setHeaders: {
                            'Content-Type': `application/json`,
                            'Authorization': `Bearer ${token}`
                        }
                    });
                    // Se comprueba si el token ha caducado o no para hacer el refresh.
                    this.authService.refreshToken(token);
                }

                // Gestión de los errores
                return next.handle(clone).pipe(
                    catchError(error => {
                        this.errorHandler.handleError(error);
                        return _throw(error);
                    })
                )
            });
    }


}