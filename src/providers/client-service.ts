import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../model/constants';
import { Client } from '../model/Client';
import { Observable } from 'rxjs/Observable';
import { ClientFilter } from '../model/ClientFilter';

@Injectable()
export class ClientServiceProvider {

  constructor(public httpClient: HttpClient) {
  }

  createClient(client: Client): Observable<Client> {
    return this.httpClient.post<Client>(Constants.ENDPOINT_SAVE_CLIENT, client);
  }

  modifyClient(client: Client): Observable<Client> {
    return this.httpClient.put<Client>(Constants.ENDPOINT_MODIFY_CLIENT, client);
  }

  checkClientExists(email: string): Observable<Client> {
    return this.httpClient.get<Client>(Constants.ENDPOINT_EXIST_CLIENT + email);
  }

  lockClient(email: string): Observable<Client> {
    return this.httpClient.post<Client>(Constants.ENDPOINT_LOCK_CLIENT + email, null);
  }

  unlockClient(email: string): Observable<Client> {
    return this.httpClient.post<Client>(Constants.ENDPOINT_UNLOCK_CLIENT + email, null);
  }

  getClientByPhone(phone: string): Observable<ClientFilter> {
    return this.httpClient.get<ClientFilter>(Constants.ENDPOINT_CLIENT_BY_PHONE + phone);
  }

}