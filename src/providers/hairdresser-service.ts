import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../model/constants';
import { Observable } from 'rxjs/Observable';
import { Hairdresser } from '../model/Hairdresser';
import { Appointment } from '../model/Appointment';
import { HairdresserAppointmentFilter } from '../model/HairdresserApointentFilter';
import { Holiday } from '../model/Holiday';

@Injectable()
export class HairdresserServiceProvider {

  constructor(private httpClient: HttpClient) {
  }

  getHairdressers(): Observable<Hairdresser[]> {
    return this.httpClient.get<Hairdresser[]>(Constants.ENDPOINT_HAIRDRESSERS);
  }

  getAppointmentsByHairdresserAndDate(filter: HairdresserAppointmentFilter): Observable<Appointment[]> {
    return this.httpClient.post<Appointment[]>(Constants.ENDPOINT_HAIRDRESSER_APPOINTMENT, filter);
  }

  getHairdresser(id: number): Observable<Hairdresser> {
    return this.httpClient.get<Hairdresser>(Constants.ENDPOINT_HAIRDRESSER_ID + id);
  }


}