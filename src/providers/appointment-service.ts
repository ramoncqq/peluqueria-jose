import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from '../model/constants';
import { Observable } from 'rxjs/Observable';
import { AppointmentCreate } from '../model/AppointmentCreate';
import { Appointment } from '../model/Appointment';
import { AppointmentCancelMassive } from '../model/AppointmentCancelMassive';

@Injectable()
export class AppointmentServiceProvider {

  constructor(private httpClient: HttpClient) { }

  createAppointment(appointmentCreate: AppointmentCreate, isadmin: boolean): Observable<AppointmentCreate> {
    let urlCreateAppointment = Constants.ENDPOINT_CREATE_APPOINTMENT + "?isAdmin=" + isadmin;
    return this.httpClient.post<AppointmentCreate>(urlCreateAppointment, appointmentCreate);
  }

  getAppointmentsByEmail(email: string): Observable<Appointment[]> {
    return this.httpClient.get<Appointment[]>(Constants.ENDPOINT_APPOINTMENT_BY_EMAIL + email);
  }

  cancelAppointment(idAppointment: number): Observable<Appointment> {
    return this.httpClient.post<Appointment>(Constants.ENDPOINT_CANCEL_APPOINTMENT + idAppointment, null);
  }

  finishAppointment(idAppointment: number): Observable<Appointment> {
    return this.httpClient.post<Appointment>(Constants.ENDPOINT_FINISH_APPOINTMENT + idAppointment, null);
  }

  getAppointmentByPhone(phone: string): Observable<Appointment[]> {
    return this.httpClient.get<Appointment[]>(Constants.ENDPOINT_APPOINTMENT_BY_PHONE + phone);
  }

  cancelAllAppointments(appointmentCancelMassive: AppointmentCancelMassive): Observable<Appointment[]> {
    return this.httpClient.post<Appointment[]>(Constants.ENDPOINT_CANCEL_MASSIVE_APPOINTMENT, appointmentCancelMassive);
  }

}
