import { Injectable } from "@angular/core";
import { ToastController, AlertController } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import * as Constants from '../model/constants';

@Injectable()
export class NotificationServiceProvider {

    constructor(
        private toastCtrl: ToastController,
        private alertCtrl: AlertController,
        private translateService: TranslateService
    ) { }

    /**
     * Método que muestra un mensaje
     * @param message 
     * @param translate 
     */
    showAlertToast(message: string, translate: boolean) {
        message = (translate) ? this.translateService.instant(message) : message;
        let toast = this.toastCtrl.create({
            message: message,
            duration: Constants.TIME_ALERT,
            position: 'bottom'
        });

        toast.present();
    }

    /**
     * Método que muestra un mensaje y si se ok llama al handler
     * @param title 
     * @param message 
     * @param handler 
     * @param translate 
     */
    showAlert(title: string, message: string, handler, translate?: boolean) {
        title = (translate) ? this.translateService.instant(title) : title;
        message = (translate) ? this.translateService.instant(message) : message;
        let alert = this.alertCtrl.create({
            title: this.translateService.instant(title),
            message: this.translateService.instant(message),
            buttons: [
                {
                    text: 'OK',
                    handler: handler
                }
            ]
        });
        alert.present();

        // En caso de que no se pulse en el botón OK hace lo mismo
        alert.onDidDismiss((data: any, role: string) => {
            handler();
        });
    }

    /**
     * Método que muestra un mensaje de confirmación y si es ok llama al handler
     * @param title 
     * @param message 
     * @param handler 
     * @param translate 
     */
    showConfirm(title: string, message: string, handler) {
        let alert = this.alertCtrl.create({
            title: this.translateService.instant(title),
            message: this.translateService.instant(message),
            buttons: [
                {
                    text: this.translateService.instant('BUTTON-CANCEL')
                },
                {
                    text: 'OK',
                    handler: handler
                }
            ]
        });
        alert.present();
    }

}