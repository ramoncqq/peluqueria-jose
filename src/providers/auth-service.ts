import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import * as Constants from '../model/constants';
import * as moment from 'moment';
import { Client } from "../model/Client";
import { TokenData } from "../model/TokenData";
import { HttpClient } from '@angular/common/http';
import { Token } from "../model/Token";
import { Observable } from "rxjs";
import { App } from 'ionic-angular';

@Injectable()
export class AuthServiceProvider {

    constructor(
        private storage: Storage,
        private httpClient: HttpClient,
        private app: App
    ) { }

    /**
     * Método que obtiene el token de la cache
     */
    getToken() {
        return this.storage.get(Constants.KEY_TOKEN);
    }

    /**
     * Método que guarda el token en la cache
     * @param token 
     */
    setToken(token: string) {
        this.storage.set(Constants.KEY_TOKEN, token);
    }

    /**
     * Método que elimina el token de la cache
     */
    deleteToken() {
        this.storage.remove(Constants.KEY_TOKEN);
    }

    /**
    /* Método que comprueba si el token ha expirado 
    /* @param token 
    */
    isTokenExpired(token: string): boolean {
        let result = true;
        if (token != null) {
            let tokenData: TokenData = JSON.parse(window.atob(token.split('.')[1]));
            let dateExpired = moment.unix(tokenData.exp).format(Constants.FORMAT_DATE_WITH_MINUTE);
            let differenceInMinutes: number = moment(dateExpired).diff(moment().format(Constants.FORMAT_DATE_WITH_MINUTE), 'minutes');
            result = (differenceInMinutes <= Constants.MAX_TIME_TOKEN) ? true : false;
        }
        return result;
    }

    /**
     * Método que comprueba si hay que refrescar el token
     * @param token 
     */
    refreshToken(token: string) {
        if (token != null) {
            let result = this.isTokenExpired(token);
            if (result) {
                this.refresh(new Token(token)).subscribe(data => {
                    this.setToken(data.token);
                });
            };
        }
    }

    /**
     * Método que dice si el usuario es administrador
     * @param token  
     */
    isAdmin(token: string) {
        let tokenData: TokenData = JSON.parse(window.atob(token.split('.')[1]));
        return (tokenData.authorities.indexOf('ADMIN') >= 0) ? true : false;
    }

    /**
     * Método que obtiene el usuario del token 
     * @param token 
     */
    getUser(token: string) {
        let tokenData: TokenData = JSON.parse(window.atob(token.split('.')[1]));
        return tokenData.sub;
    }

    /**
     * Método que realiza la petición para el login
     */
    login(client: Client): Observable<Token> {
        return this.httpClient.post<Token>(Constants.ENDPOINT_LOGIN, client);
    }

    /**
     * Método que realiza la petición para refrescar el token
     * @param token
     */
    refresh(token: Token): Observable<Token> {
        return this.httpClient.post<Token>(Constants.ENDPOINT_REFRESH, token);
    }

    /**
     * Método que hace el login y el redirect al tabs page.
     * @param client 
     */
    loginAndRedirect(client: Client) {
        this.login(client).subscribe((data: Token) => {
            this.setToken(data.token);
            if (this.isAdmin(data.token)) {
                this.app.getRootNavs()[0].setRoot('TabsPage', { client: client, opentab: Constants.TAB_APPOINTMENTS_ADMIN });
            } else {
                this.app.getRootNavs()[0].setRoot('TabsPage', { client: client, opentab: Constants.TAB_APPOINTMENTS_USER });
            }
        });
    }

}