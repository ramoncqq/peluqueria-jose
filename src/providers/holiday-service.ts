import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from '../model/constants';
import { Observable } from 'rxjs/Observable';
import { Hairdresser } from '../model/Hairdresser';
import { Appointment } from '../model/Appointment';
import { HairdresserAppointmentFilter } from '../model/HairdresserApointentFilter';
import { Holiday } from '../model/Holiday';

@Injectable()
export class HolidayServiceProvider {

  constructor(private httpClient: HttpClient) {
  }

  getHolidaysHairdresser(idHairdresser: number): Observable<Holiday[]> {
    return this.httpClient.get<Holiday[]>(Constants.ENDPOINT_HOLIDAY + `/${idHairdresser}`);
  }

  createHolidays(holiday: Holiday[]): Observable<Holiday[]> {
    return this.httpClient.post<Holiday[]>(Constants.ENDPOINT_HOLIDAY, holiday);
  }

  cancelHolidays(holiday: Holiday[]): Observable<any> {
    return this.httpClient.put<any>(Constants.ENDPOINT_HOLIDAY, holiday);
  }

}