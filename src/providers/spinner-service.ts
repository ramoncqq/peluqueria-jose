import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SpinnerServiceProvider {

  loading: any;

  constructor(
    private loadingCtrl: LoadingController,
    private translateService: TranslateService
  ) { }

  show() {
    this.loading = this.loadingCtrl.create({
      content: this.translateService.instant('SPINNER-TITLE')
    });
    this.loading.present();
  }

  hide() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

}
