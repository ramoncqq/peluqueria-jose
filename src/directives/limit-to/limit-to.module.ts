import { NgModule } from '@angular/core';
import { LimitToDirective } from './limit-to';
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [LimitToDirective],
	imports: [IonicModule],
	exports: [LimitToDirective]
})
export class LimitToModule {}
