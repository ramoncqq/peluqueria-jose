import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Client } from "../../model/Client";

@Component({
  selector: 'form-client',
  templateUrl: 'form-client.html'
})
export class FormClientComponent implements OnInit {

  @Input("client") client: Client;
  @Output() sendFormListener = new EventEmitter();

  formClient: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formClient = this.initializeForm();
  }

  sendForm() {
    this.sendFormListener.emit(this.formClient);
  }

  private initializeForm() {
    return this.formBuilder.group({
      name: [this.client.name, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      surname: [this.client.surname, [Validators.required, Validators.minLength(10), Validators.maxLength(30)]],
      phone: [this.client.phone, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      email: [this.client.email, []],
      notification: [this.client.notification]
    });
  }

}
