import { NgModule } from '@angular/core';
import { FormClientComponent } from './form-client';
import { IonicModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        FormClientComponent,
    ],
    imports: [
        IonicModule,
        TranslateModule
    ],
    exports: [
        FormClientComponent
    ]
})
export class FormClientModule { }