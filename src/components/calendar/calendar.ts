import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { CalendarServiceProvider } from "../../providers/calendar-service";
import { CalendarComponentOptions, DayConfig } from "ion2-calendar";
import * as moment from 'moment';
import { AvailabilityServiceProvider } from "../../providers/availabity-service";
import * as Constants from '../../model/constants';

@Component({
  selector: 'calendar-component',
  templateUrl: 'calendar.html'
})
export class CalendarCustomizeComponent implements OnInit {

  @Input("maxTime") maxTime: number = 2;
  @Input("availability") availability : boolean = false;
  
  @Output() sendEventCalendar: EventEmitter<any> = new EventEmitter<any>();

  // Variables del calendario
  date: string;
  type: 'moment';
  optionsCalendar: CalendarComponentOptions;
  daysConfig: DayConfig[];

  constructor(
    private calendarService: CalendarServiceProvider,
    private availabilityService: AvailabilityServiceProvider
  ) { }

  ngOnInit() {
    debugger;
    console.log(`DISPONIBILIDAD EN EL ON INIT: ${this.availability}, con el tiempo limite : ${this.maxTime}`);
    (this.availability) ? this.initCalendarWithAvailability() : this.initCalendar();
  }

  // Se obtiene la lista de dispo y se crean los dias de configuración para el calendario
  public initCalendarWithAvailability() {
     this.daysConfig = this.calendarService.getDaysConfigCalendar(this.availabilityService.getAvailabilityList());
    this.initCalendar();
  }

  // Emite el evento hacia el padre parseando la fecha a YYY-MM-DD
  public emitChangeCalendar(date) {
    this.sendEventCalendar.emit(moment(date).format(Constants.FORMAT_DATE));
  }

  // Se inicializa el calendario
  private initCalendar() {
    this.optionsCalendar = {
      showAdjacentMonthDay: false,
      weekStart: 1,
      weekdays: this.calendarService.getDaysWeek(),
      monthPickerFormat: this.calendarService.getMonths(),
      showMonthPicker: false,
      daysConfig: this.daysConfig,
      to: moment().add(this.maxTime, 'M').toDate()
    }
  }

}