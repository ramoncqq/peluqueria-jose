import { NgModule } from '@angular/core';
import { CalendarCustomizeComponent } from './calendar';
import { CalendarModule } from 'ion2-calendar';
import { IonicModule } from 'ionic-angular';

@NgModule({
    declarations: [

        CalendarCustomizeComponent,
    ],
    imports: [
        IonicModule,
        CalendarModule
    ],
    exports: [
        CalendarCustomizeComponent
    ]
})
export class CalendarCustomModule { }