import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular/';
import { FcmServiceProvider } from '../../providers/fcm';

@Component({
    selector: 'notification',
    templateUrl: 'notification.html'
})
export class NotificationComponent implements OnInit {

    notificationList: Array<string>;

    constructor(
        private navParams: NavParams,
        public viewCtrl: ViewController,
        private fcmServiceProvider: FcmServiceProvider
    ) { }

    ngOnInit() {
        this.notificationList = this.navParams.get('notificationList');
    }

    /**
     * Método que emite el evento hacia el componente de la cabecera
     */
    closeModal() {
        this.viewCtrl.dismiss();
    }

    /**
     * Método que elimina una notificación de la lista
     * @param id 
     **/
    deleteNotification(id: number) {
        this.fcmServiceProvider.notificationsList.splice(id, 1);
    }

}
