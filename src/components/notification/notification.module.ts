import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationComponent } from './notification';

@NgModule({
    declarations: [
        NotificationComponent,
    ],
    imports: [
        IonicModule,
        TranslateModule
    ],
    exports: [
        NotificationComponent
    ],
    entryComponents: [
        NotificationComponent
    ]
})
export class NotificationModule { }