import { Component, OnInit } from '@angular/core';
import { App } from 'ionic-angular/components/app/app';
import { AuthServiceProvider } from '../../providers/auth-service';
import { ModalController, Modal } from 'ionic-angular';
import { NotificationComponent } from '../notification/notification';
import { FcmServiceProvider } from '../../providers/fcm';

@Component({
  selector: 'app-header',
  templateUrl: 'header.html'
})
export class HeaderComponent implements OnInit {

  logged: boolean;
  modal: Modal;
  notificationList: Array<string> = [];

  constructor(
    private app: App,
    private authService: AuthServiceProvider,
    private modalCtrl: ModalController,
    private fcmServiceProvider: FcmServiceProvider
  ) { }

  ngOnInit() {
    let logged = false;
    this.authService.getToken().then(data => {
      if (data != null) {
        this.logged = (this.authService.isTokenExpired(data)) ? false : true;
      }
    });
    this.logged = logged;
    this.notificationList = this.fcmServiceProvider.notificationsList;
  }

  /**
   * Método que borra el token y redirecciona a la pantalla del login
   */
  closeSession() {
    this.authService.deleteToken();
    this.app.getRootNavs()[0].setRoot('LoginPage');
  }

  /**
   * Método que abre el menu de las notificaciones
   */
  showNotifications() {
    let modal = this.modalCtrl.create(NotificationComponent, { notificationList: this.notificationList });
    modal.present();
  }

}
