import { NgModule } from '@angular/core';
import { HeaderComponent } from './header';
import { IonicModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationModule } from '../notification/notification.module';

@NgModule({
    declarations: [
        HeaderComponent,
    ],
    imports: [
        IonicModule,
        TranslateModule,
        NotificationModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule { }