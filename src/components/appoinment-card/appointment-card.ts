import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Appointment } from '../../model/Appointment';
import { AuthServiceProvider } from '../../providers/auth-service';
import { AppointmentServiceProvider } from '../../providers/appointment-service';
import { NotificationServiceProvider } from '../../providers/notification-service';
import * as Constants from '../../model/constants';

@Component({
    selector: 'appointment-card',
    templateUrl: 'appointment-card.html',
    styles: ['style.scss']
})
export class AppointmentCard implements OnInit {

    @Input("appointments") appointments: Array<Appointment>;
    @Output() finishAppointmentListener = new EventEmitter<any>();
    isAdmin: boolean;
    email: string;
    id: number;

    constructor(
        private authService: AuthServiceProvider,
        private appointmentService: AppointmentServiceProvider,
        private notificationService: NotificationServiceProvider) { }

    // Funcion que comprueba si el usuario es un administrador.
    ngOnInit() {
        this.authService.getToken().then(data => {
            this.isAdmin = this.authService.isAdmin(data);
            console.log(`COMPONENETE TARJETA: ISADMIN: ${this.isAdmin}`);
        });
    }

    // Función que emite el evento para finalizar la cita
    emitFinishAppointment(id: number) {
        this.finishAppointmentListener.emit(id);
    }

    // Función que muestra la confirmación para cancelar la cita
    showConfirmCancelAppointment(id: number) {
        this.id = id;
        this.notificationService.showConfirm('CANCEL-APPOINTMENT', 'CANCEL-APPOINTMENT-MESSAGE', this.cancelAppointment.bind(this));
    }

    // Función que cancela una cita
    cancelAppointment() {
        this.appointmentService.cancelAppointment(this.id).subscribe(data => {
            this.appointments.filter(appointment => appointment.id === this.id)[0].state = Constants.STATE_CANCEL;
            this.notificationService.showAlertToast('CANCEL-APPOINTMENT-MESSAGE-OK', true);
        });
    }
}