import { NgModule } from '@angular/core';
import { AppointmentCard } from './appointment-card';
import { IonicModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        AppointmentCard,
    ],
    imports: [
        IonicModule,
        TranslateModule
    ],
    exports: [
        AppointmentCard
    ]
})
export class AppointmentCardModule { }