import { Component } from '@angular/core';
import { App, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { LoginPage } from '../pages/login/login';
import { FcmServiceProvider } from '../providers/fcm';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import * as moment from 'moment';
import { AuthServiceProvider } from '../providers/auth-service';
import { ClientServiceProvider } from '../providers/client-service';
import * as Constants from '../model/constants';
import { NotificationServiceProvider } from '../providers/notification-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any = LoginPage;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private translateService: TranslateService,
    private fcmService: FcmServiceProvider,
    private clientService: ClientServiceProvider,
    private notificationService: NotificationServiceProvider,
    private app: App,
    private authService: AuthServiceProvider,
    private screenOrientation: ScreenOrientation) {
    this.platform.ready().then(() => {

      this.initializeApp();

      this.statusBar.hide();
      this.splashScreen.hide();
    });
  }

  private initializeApp() {

    // Se configura el idioma de la aplicación y el de moment
    this.translateService.setDefaultLang('es');
    moment.locale("es");

    this.controlSession();

    // Permitir la pantalla en modo vertical
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    // Se obtienen las notificaciones PUSH
    this.fcmService.listenToNotifications().subscribe(data => {
      console.log(`Se obtiene la notifcation ${JSON.stringify(data)}`);
      this.fcmService.notificationsList.push(data.body);
    }, error => {
      console.log(`Error al obtner las notificaciones de firebase : ${error}`);
    });

  }

  /**
   * Método que controla la sesión
   */
  private controlSession() {
    console.log('init session');
    this.authService.getToken().then(token => {
      if (token != null) {
        // Si el token no ha expirado se carga el cliente del token y se le redirecciona al home
        if (!this.authService.isTokenExpired(token)) {
          this.clientService.checkClientExists(this.authService.getUser(token)).subscribe(data => {
            if (this.authService.isAdmin(token)) {
              this.app.getRootNavs()[0].setRoot('TabsPage', { client: data, opentab: Constants.TAB_APPOINTMENTS_ADMIN });
            } else {
              this.app.getRootNavs()[0].setRoot('TabsPage', { client: data, opentab: Constants.TAB_APPOINTMENTS_USER });
            }
          });
        } else {
          this.authService.deleteToken();
        }
      }
    });
  }

}

