
// Modules ThirdParty
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CalendarModule } from "ion2-calendar";
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { Firebase } from '@ionic-native/firebase';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpsRequestInterceptor } from '../providers/http-interceptor';
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from '../providers/auth-service';
import { FcmServiceProvider } from '../providers/fcm';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

// IMPORT PROVIDERS 
import { AppointmentServiceProvider } from '../providers/appointment-service';
import { ClientServiceProvider } from '../providers/client-service';
import { NotificationServiceProvider } from '../providers/notification-service';
import { HairdresserServiceProvider } from '../providers/hairdresser-service';
import { CalendarServiceProvider } from '../providers/calendar-service';
import { AvailabilityServiceProvider } from '../providers/availabity-service';
import { SpinnerServiceProvider } from '../providers/spinner-service';

// Import modules
import { LoginPageModule } from '../pages/login/login.module';
import { TabsPageModule } from '../pages/tabs/tabs.module';
import { ErrorsHandler } from '../providers/errors-service';
import { HolidayServiceProvider } from '../providers/holiday-service';

export function setTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const firebase = {
  apiKey: "AIzaSyA2NMzhCETL2d1clPIxgUxSHjZ5cbWVKH4",
  authDomain: "https://peluqueria-jose-a1e8f.firebaseio.com",
  projectId: "peluqueria-jose-a1e8f",
  storageBucket: "peluqueria-jose-a1e8f.appspot.com",
  messagingSenderId: "310223589546"
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (setTranslateLoader),
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    CalendarModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule,
    // Modules Propios
    LoginPageModule,
    TabsPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    ScreenOrientation,
    GooglePlus,
    ErrorsHandler,
    AppointmentServiceProvider,
    ClientServiceProvider,
    NotificationServiceProvider,
    HairdresserServiceProvider,
    HolidayServiceProvider,
    AuthServiceProvider,
    CalendarServiceProvider,
    Firebase,
    FcmServiceProvider,
    AvailabilityServiceProvider,
    SpinnerServiceProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
  ]
})
export class AppModule { }
